let baseUrl="http://api-admin.wash-dev.tenyuanda.com/md";


export const washList      = baseUrl+"/washer/v1/findAll";
export const washDelete = baseUrl+"/washer/v1/delete";
export const washAdd     = baseUrl+"/washer/v1/add";
export const washUpdate     = baseUrl+"/washer/v1/update";
export const washSelect =baseUrl+'/washer/v1/findWasherById?id=';


export const priceAdd = baseUrl+'/pricing/v1/add';
export const priceDelete = baseUrl+'/pricing/v1/delete';
export const priceList = baseUrl+'/pricing/v1/findAll';
export const priceUpdate = baseUrl+'/pricing/v1/update';
export const priceSelect = baseUrl+'/pricing/v1/findPricingById?id=';
