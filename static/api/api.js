import axios from 'axios'
import * as url from './url'
//import config from './config'


export const Status = {
  codeStatus(response){
    if(response.status=="200"){
      return response.data;

    }
  }
};
export const WashListApi ={

  washList(){
    return axios.post(url.washList)
      .then(response =>{
        return Status.codeStatus(response)
      }).catch(error=>{
        console.log(error)
      })
  },
  add(param){
    return axios.post(url.washAdd,param)
      .then(response =>{
        return Status.codeStatus(response)
      }).catch(error=>{
        console.log(error)
      })
  },
  update(param){
    return axios.post(url.washUpdate,param)
      .then(response =>{
        return Status.codeStatus(response)
      }).catch(error=>{
        console.log(error)
      })
  },
  delete(param){
    return axios.post(url.washDelete,param)
      .then(response =>{
        return Status.codeStatus(response)
      }).catch(error=>{
        console.log(error)
      })
  },
  select(param){
      return axios.post(url.washSelect+param)
          .then(response =>{
              return Status.codeStatus(response)
          }).catch(error=>{
              console.log(error)
          })
  }
};

export const PriceApi={
    priceList(){
        return axios.post(url.priceList)
            .then(response =>{
                return Status.codeStatus(response)
            }).catch(error=>{
                console.log(error)
            })
    },
    priceSelect(id){
        return axios.post(url.priceSelect+id)
            .then(response =>{
                return Status.codeStatus(response)
            }).catch(error=>{
                console.log(error)
            })
    },
    priceAdd(param){
        return axios.post(url.priceAdd,param)
            .then(response =>{
                return Status.codeStatus(response)
            }).catch(error=>{
                console.log(error)
            })
    },
    priceEdit(param){
        return axios.post(url.priceUpdate,param)
            .then(response =>{
                return Status.codeStatus(response)
            }).catch(error=>{
                console.log(error)
            })
    },
    priceDelete(param){
        return axios.post(url.priceDelete,param)
            .then(response =>{
                return Status.codeStatus(response)
            }).catch(error=>{
                console.log(error)
            })
    },
}
