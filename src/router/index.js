import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',

      component: (resolve) => require(['../components/index.vue'], resolve),

      children: [
        {
          path: '/',
          component: (resolve) => require(['../components/wash/wash-list.vue'], resolve),
        },
        {
          path: '/washlist',
          component: (resolve) => require(['../components/wash/wash-list.vue'], resolve),
        },
        {
          path: '/washAdd',
          component: (resolve) => require(['../components/wash/wash-add.vue'], resolve),
        },
        {
          path: '/washEdit',
          component: (resolve) => require(['../components/wash/wash-edit.vue'], resolve),
        },
          {
              path: '/priceList',
              component: (resolve) => require(['../components/price/price-list.vue'], resolve),
          },
          {
              path: '/priceAdd',
              component: (resolve) => require(['../components/price/price-add.vue'], resolve),
          },
          {
              path: '/priceEdit',
              component: (resolve) => require(['../components/price/price-edit.vue'], resolve),
          }
      ]

    },

  ]
})
